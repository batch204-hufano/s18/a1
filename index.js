// console.log("Working");.

/*
1. Results of functions to show
sum and difference of two
numbers on the console.
*/

function getSum (num1, num2) {

	console.log("Displayed sum of " + num1 + " and " + num2);
	console.log(num1 + num2);
};

getSum (5, 15);

/*********************/

function getDifference (num1, num2) {

	console.log("Displayed sum of " + num1 + " and " + num2);
	console.log(num1 - num2);
};

getDifference (20,5);

/*
2. Values of variables that store
the returned values of functions
that multiplies and divides two
numbers.
*/

function getProduct (num1, num2) {

	console.log("The product of " + num1 + " and " + num2);
	return num1 * num2;

};

let totalProduct = getProduct (50, 10);

console.log(totalProduct);

/******************************/

function getQuotient (num1, num2) {

	console.log("The quotient of " + num1 + " and " + num2);
	return num1 / num2;

};

let totalQuotient = getQuotient (50, 10);

console.log(totalQuotient);

/*
3. Values of variable that store the
returned value of function able
to calculate area of a circle by
its radius
*/

function getAreaOfCircle (radius) {

	console.log("The result of getting the are of a circle with " + radius + " radius:");
	return (3.14159 * (radius ** 2));
}

let totalAreaofCircle = getAreaOfCircle(15);
console.log(totalAreaofCircle);

/*
4. Value of variable which stores
the result of a function that
calculates the average of 4
numbers.
*/

function getTheAverageOfNumbers (num1, num2, num3, num4) {

		console.log("The average of " + num1 + "," + num2 + "," + num3 + "," + num4 + ":");
		return (num1 + num2 + num3 + num4) / 4;
};

let averageVar = getTheAverageOfNumbers(20, 40, 60, 80);
console.log(averageVar);

/*
5. Value of variable which stores
the result of a function that
checks if a scoring percentage
is passed or failed.
*/

function getPassingPercentage (score, totalScore) {

	console.log("Is " + score + "/" + totalScore + " a passing score?");

	let getPercentage = (score / totalScore) * 100;
	let passingPercentage = 75;
	let isPassed = getPercentage > passingPercentage;

	return isPassed;
};
let isPassingScore = getPassingPercentage(38,50);

console.log (isPassingScore);